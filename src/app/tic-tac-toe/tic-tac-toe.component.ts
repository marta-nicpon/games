import { Component, OnInit } from '@angular/core';
import { FieldPosition } from './fileldPosition';
import { VictoryStatus } from './victory-status';

@Component({
  selector: 'app-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.css']
})
export class TicTacToeComponent implements OnInit {

  // {} robi nowy obiekt, obiekty przyporządkowują kluczom wartości
  // <{ number; string }> przed {} mówi, że klucze są typu number, a wartości typu string
  indexToOwner: { number; string };
  rowNames = ['1', '2', '3'];
  columnNames = ['A', 'B', 'C'];
  blocked: boolean; // nie jest zablokowany
  victoryStatus: VictoryStatus;

  constructor() {
    this.startGame();
  }

  ngOnInit() { }

  getFieldIndex(x: number, y: number) {
    return y * 3 + x;
  }

  fieldClick(x: number, y: number) {
    const playerMove = this.getFieldIndex(x, y);
    if (!this.blocked && !this.indexToOwner[playerMove]) {
      this.blocked = true;
      this.indexToOwner[playerMove] = 'X';
      this.checkWin(playerMove);
      /* po wpisaniu x, komputer automatycznie generuje o, wywołam metodę */
      this.makeComputerMove();
    }
  }

  makeComputerMove() {
    if (!this.victoryStatus) {
      const allowedMoves: number[] = [];
      for (let fieldIndex = 0; fieldIndex < 9; fieldIndex++) {
        if (!this.indexToOwner[fieldIndex]) {
          allowedMoves.push(fieldIndex);
        }
      }
      window.setTimeout(() => { // 2 argumenty: co zrobić; za ile milisekund to zrobić
        if (allowedMoves.length > 0) {
          const computerMove = allowedMoves[Math.floor(Math.random() * allowedMoves.length)];
          this.indexToOwner[computerMove] = 'O';
          this.checkWin(computerMove);
          this.blocked = false;
        } else {
          this.victoryStatus = VictoryStatus.draw;
        }
      }, 1000);
    }
  }

  checkWin(currentMove: number) {
    const currentOwner = this.indexToOwner[currentMove];
    const positionX = currentMove % 3;
    const positionY = Math.floor(currentMove / 3);

    let match = true;
    for (let y = 0; y < 3; y++) {
      if (this.indexToOwner[this.getFieldIndex(positionX, y)] !== currentOwner) {
        match = false;
        break;
      }
    }
    if (!match) {
      match = true;
      for (let x = 0; x < 3; x++) {
        if (this.indexToOwner[this.getFieldIndex(x, positionY)] !== currentOwner) {
          match = false;
          break;
        }
      }
    }
    /* diagonalnie */
    if (!match) {
      match = true;
      for (let i = 0; i < 3; i++) {
        if (this.indexToOwner[this.getFieldIndex(i, i)] !== currentOwner) {
          match = false;
          break;
        }
      }
    }
    if (!match) {
      match = true;
      for (let i = 0; i < 3; i++) {
        if (this.indexToOwner[this.getFieldIndex(i, 2 - i)] !== currentOwner) {
          match = false;
          break;
        }
      }
    }
    if (match) {

      window.setTimeout(() => {
        if (currentOwner === 'X') {
          this.victoryStatus = VictoryStatus.winner;
        } else {
          this.victoryStatus = VictoryStatus.looser;
        }
      }, 1000);
    }
    // alert('V for Victory :)');
  }

  startGame() {
    this.indexToOwner = <{ number; string }>{};
    this.blocked = false; // nie jest zablokowany
    this.victoryStatus = null;

  }

}
