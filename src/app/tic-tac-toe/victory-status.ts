export enum VictoryStatus {
  winner = 'w',
  looser = 'l',
  draw = 'd',
}
