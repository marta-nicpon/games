import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemoryComponent } from './memory/memory.component';
import { TicTacToeComponent } from './tic-tac-toe/tic-tac-toe.component';
import { GameListComponent } from './game-list/game-list.component';
import { KulkiComponent } from './kulki/kulki.component';


const routes: Routes = [
  { path: '', component: GameListComponent },
  { path: 'memory', component: MemoryComponent },
  { path: 'tic-tac-toe', component: TicTacToeComponent },
  { path: 'kulki', component: KulkiComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
