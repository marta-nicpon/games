import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MemoryComponent } from './memory/memory.component';
import { TicTacToeComponent } from './tic-tac-toe/tic-tac-toe.component';
import { AppRoutingModule } from './/app-routing.module';
import { GameListComponent } from './game-list/game-list.component';
import { CardComponent } from './memory/card/card.component';
import { CongratulationComponent } from './shared/congratulation/congratulation.component';
import { KulkiComponent } from './kulki/kulki.component';

@NgModule({
  declarations: [
    AppComponent,
    MemoryComponent,
    TicTacToeComponent,
    GameListComponent,
    CongratulationComponent,
    CardComponent,
    KulkiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
