import { Component } from '@angular/core';
import { Card } from './card';

@Component({
  selector: 'app-root',
  templateUrl: './memory.component.html',
  styleUrls: ['./memory.component.css']
})
export class MemoryComponent {

  board: Card[][] = []; // [][] lista dwuwymiarowa
  firstChosenCard: Card;
  blocked = false;
  rows = 4;
  columns = 4;
  cardsLeft: number;

  constructor() { // stan począkowy :-)

    const urlList: string[] = ['assets/oranges.jpg', 'assets/lime.jpg', 'assets/fruit.jpg', 'assets/strawberries.jpg',
      'assets/background.jpg', 'assets/background2.jpg', 'assets/bananas.jpg', 'assets/mango.jpg'];
    this.cardsLeft = this.rows * this.columns;
    let cards: Card[] = [];
    for (let i = 0; i < this.rows * this.columns; i++) {
      const card = new Card();
      cards.push(card);
      card.imageUrl = urlList[i % urlList.length]; // dzielenie modulo
      // reszta dzielenia przez 8 sprawi, że każda karta będzie wyświetlana dwa razy,
      // alternatywa to dwie pętle
      card.reversed = true;
      //  card.reversed = i % 2 === 1; // co drugi
    }

    /*** zasymulowanie metody do tasowania kart ***/
    // do każdego elemenntu listy dodaje losową liczbę i sortuję
    cards = cards
      // dla każdego el z listy wywoła funkcję i zwróci listę wyników.
      // właściwości w tym objekcie możemy dać tyle ile chcemy
      // kartę przemapowujemy sobie kartę na parę informacji: kasrta + losowa liczba
      .map(card => ({ randomNumber: Math.random(), card: card }))
      // sortuje listę, jako argument przyjmuje funkcje która ma porównać dwa elementy tej listy
      // jeśli a>b zwróci dodatnią, jeśli b<a ujemną, jeśli a=b 0
      .sort((a, b) => a.randomNumber - b.randomNumber)
      // zapominamy te losowe liczby i przemapowujemy na samą kartę
      .map(element => element.card);

    let index = 0;
    for (let row = 0; row < this.rows; row++) {
      this.board.push([]);
      for (let column = 0; column < this.columns; column++) {
        const card = cards[index]; // tutaj dopiero index nabiera sensu i wiadomo, że jest pobieraniem kolejnej karty
        card.row = row; // w karcie zapisujemy w którym jest wierszu
        card.column = column;
        this.board[row].push(card); // konkretnej pozycji przypisuje kartę
        index++;
        // this.board[row][column]  row wyciągam  board, za pomocą kropki jest przy obiektach, a przy listach tak
      }
    }
  }

  chooseCard(card: Card) {
    // jeśli niezablokowane
    if (!this.blocked && this.firstChosenCard !== card) { // dokładnie tutaj nadajemy znaczenie blocked
      card.reversed = false;
      if (this.firstChosenCard) { // będzie istnieć tylko po elsie
        // zablokuj
        this.blocked = true;
        // przed strzałką argumenty, puste () oznaczają że nie ma argumentów, wąsy - blok poleceń
        window.setTimeout(() => { // 2 argumenty: co zrobić; za ile milisekund to zrobić
          if (this.firstChosenCard.imageUrl === card.imageUrl) {
            this.board[card.row][card.column] = null; // karta już nie będzie na tym polu
            this.board[this.firstChosenCard.row][this.firstChosenCard.column] = null;
            this.cardsLeft = this.cardsLeft - 2;

          } else {
            card.reversed = true;
            this.firstChosenCard.reversed = true;
          }
          this.firstChosenCard = null; // sprawi że znowu jesteśmy w tym trybie, że wybieramy pierwszą kartę
          // odblokuj
          this.blocked = false;
        }, 1000);
      } else {
        this.firstChosenCard = card;
      }
    }
  }
  }

