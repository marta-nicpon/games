export class Card {
  imageUrl: string;
  reversed: boolean;
  row: number;
  column: number;
}
