import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-congratulation',
  templateUrl: './congratulation.component.html',
  styleUrls: ['./congratulation.component.css']
})
export class CongratulationComponent implements OnInit {

  @Input() victoryStatus;
  @Output() newGame = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
