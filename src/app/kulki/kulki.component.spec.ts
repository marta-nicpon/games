import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KulkiComponent } from './kulki.component';

describe('KulkiComponent', () => {
  let component: KulkiComponent;
  let fixture: ComponentFixture<KulkiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KulkiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KulkiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
