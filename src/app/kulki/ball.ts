export class Ball {

  x: number;
  y: number;
  color: string;
  group = [this];

  constructor(x: number, y: number, color: string) { // ten kto tworzy piłkę powiedział te dane
    this.x = x;
    this.y = y;
    this.color = color;
  }
}
