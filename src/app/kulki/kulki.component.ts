import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { Ball } from './ball';
import { VictoryStatus } from './vistory-status';



@Component({
  selector: 'app-kulki',
  templateUrl: './kulki.component.html',
  styleUrls: ['./kulki.component.css']
})
/** AfterViewInit - jest po to, żebyśmy mogli kazać zrobić coś komputerowi jak już skończy przygotowywać komponent **/
export class KulkiComponent implements OnInit, AfterViewInit {

  /** ViewChild jest po to, żeby się dobrać do dziecka (canvas) rodzica (komponentu); @ adnotacja przekazuje dodatkowe informacje,
   są z typescripta,
   * ViewChild nazwa adnotacji; argument to nazwa pola angularowego z htmla, my Canvas po arg to jest już nazwa mojego pola **/
  @ViewChild('myCanvas') myCanvas: ElementRef<HTMLCanvasElement>; // w angularze wszystkie dzieci są typu ElementRef
  context: CanvasRenderingContext2D; // tego typu jest to coś czym się rysuje na canvasie
  radius = 16;
  colors = ['rgb(255, 255, 18)', 'rgb(255, 13, 13)', 'rgb(15, 15, 255)', 'rgb(9, 255, 9)', 'rgb(247, 0, 247)', 'rgb(27, 255, 255)'];
  board: Ball[] = [];
  row = 0;
  movingBall: Ball;
  vx: number;
  vy: number;
  boardWidth: number;
  boardHeight: number;
  totalHeight: number;
  ballsInRow = 18;
  maxRowsCount = 15;
  speed = 0.5;
  middleBall: Ball;
  nextBall: Ball;
  distanceBetweenBallsY: number = this.radius * Math.sqrt(3);
  distanceBetweenBallsX: number = this.radius * 2;
  lost = 0;
  victoryStatus: VictoryStatus;
  arrowX: number;
  arrowY: number;
  arrowAngle = 0;


  constructor() {
    this.boardWidth = this.distanceBetweenBallsX * this.ballsInRow + this.radius;
    this.boardHeight = this.distanceBetweenBallsY * this.maxRowsCount;
    this.totalHeight = this.boardHeight + 4 * this.radius;

    for (let i = 0; i < 8; i++) {
      this.addTopRaw(); // inicjalizuje informacje o kulkach, ale nic nie rysuje, dlatego może być już w konstruktorze
    }
    this.addNextBall();
    this.addMiddleBall();
  }

  ngOnInit() {
  }
  /** służy do przygotowania komponentu do pracy **/
  ngAfterViewInit(): void { // void nic nie zwraca
    this.context = this.myCanvas.nativeElement.getContext('2d');
    this.drawAll(); // musi być po canvasie, w konstruktorze nie miałabym jeszcze płótna

  }
  /** Metoda, która przerysowuje plansze **/
  drawAll() {
    this.context.clearRect(0, 0, this.myCanvas.nativeElement.width, this.myCanvas.nativeElement.height); // metoda czyszcząca

    for (const ball of this.board) {
      this.drawBall(ball);
    }
    this.drawBall(this.middleBall);
    this.drawBall(this.nextBall);

    this.context.beginPath();
    this.context.lineWidth = 1;
    this.context.strokeStyle = 'black';
    this.context.rect(0, 0, this.boardWidth, this.boardHeight);
    this.context.stroke();

    this.drawArrow();
  }

  drawArrow() {
    this.context.save();
    this.context.translate(this.arrowX, this.arrowY);
    this.context.rotate(this.arrowAngle);

    this.context.beginPath();
    this.context.moveTo(0, 0);
    this.context.lineTo(60, 0);
    this.context.lineTo(60 - 20, 10);
    this.context.moveTo(60, 0);
    this.context.lineTo(60 - 20, -10);
    this.context.stroke();

    this.context.restore();
  }

  drawBall(ball: Ball) {
    this.context.beginPath();
    this.context.arc(ball.x, ball.y, this.radius, 0, 2 * Math.PI);
    this.context.fillStyle = ball.color;
    this.context.fill();
  }

  addTopRaw() {
    for (const ball of this.board) {
      ball.y = ball.y + this.distanceBetweenBallsY;
    }

    const y = this.radius;

    let x = this.radius;
    if (this.row % 2 === 1) {
      x = x + this.radius;
    }
    for (let ball = 0; ball < 18; ball++) {
      this.addBall(new Ball(x, y, this.colors[Math.floor(Math.random() * this.colors.length)]));
      x = x + this.distanceBetweenBallsX;
    }

    this.row++;
  }

  addNextBall() {
    this.nextBall = new Ball(this.boardWidth * 0.1, this.totalHeight - this.radius,
      this.colors[Math.floor(Math.random() * this.colors.length)]);
  }

  addMiddleBall() {
    this.middleBall = this.nextBall;
    this.middleBall.x = this.boardWidth * 0.5;
    this.addNextBall();
    this.arrowX = this.middleBall.x;
    this.arrowY = this.middleBall.y;
  }

  /** Obsługa wydarzenia od kliknięcia. MoseEvent to typ z przeglądarki  **/
  click(event: MouseEvent) {
    if (!this.movingBall) {
      const rect = this.myCanvas.nativeElement.getBoundingClientRect();
      const mouseX = event.clientX - rect.left;
      const mouseY = event.clientY - rect.top;
      //  this.addTopRaw();
      // zrobienie instancji i wywołanie na niej konstruktora - to z tym new
      this.movingBall = this.middleBall;
      // tutaj w zal od znaku poleci w lewo lub prawo
      this.vx = mouseX - this.movingBall.x;
      this.vy = mouseY - this.movingBall.y;

      const voctorLength = Math.sqrt(this.vx * this.vx + this.vy * this.vy);
      // dzielenie wektorów
      this.vx = this.vx / voctorLength * this.speed;
      this.vy = this.vy / voctorLength * this.speed;

      // this.drawBoard();
      // this.drawBall(x, y, 'rgb(255, 13, 13)')
      window.requestAnimationFrame((nextTime: number) => this.animate(nextTime, nextTime));
    }
  }
  mouseMove(event: MouseEvent) {
    const rect = this.myCanvas.nativeElement.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;
    this.arrowAngle = Math.atan2(mouseY - this.arrowY, mouseX - this.arrowX);
    this.drawAll();
  }

  /** Metoda uaktualniająca pozycje lecących kulek **/
  update(deltaTime: number) {
    // zabezpieczenieżeby nic nie zmieniać, jeżeli nie upłyną żaden czas
    if (deltaTime > 0) {
      this.movingBall.y = this.movingBall.y + deltaTime * this.vy;
      this.movingBall.x = this.movingBall.x + deltaTime * this.vx;
      // this.movingBall.x = this.movingBall.x - deltaTime * this.vx;

      if (this.movingBall.x < this.radius) {
        this.vx = this.vx * -1;
        // to z tym, żeby nie lądowało na 95
        const tooFarX = this.radius - this.movingBall.x; // const tylko w klamerkach obowiązuje
        this.movingBall.x = this.radius + tooFarX;
      }
      /* Jeśli piłeczka znajdzie sę poza prawą krawędzią tj przynajmniej częściowo wyjdzie */
      if (this.movingBall.x > this.boardWidth - this.radius) {
        this.vx = this.vx * -1;

        const tooFarRightX = this.movingBall.x - (this.boardWidth - this.radius); // - this.radius bo odbija się nie środek
        this.movingBall.x = this.boardWidth - this.radius - tooFarRightX;
        // to z tym, żeby nie lądowało na 95
        // this.movingBall.x = 100 + 100 - this.movingBall.x;
      }
      // zatrzymywanie się po dojściu na samą górę
      if (this.movingBall.y < this.radius) {
        this.movingBall.y = this.radius;
        this.stopMovingBall();
        return; // wychodzi z metody
      }
      // zatrzymywanie się po zderzeniu z inną piłeczką
      for (const ball of this.board) {
        // delta to wektor od środka ball do środka movingBall
        const deltaX = this.movingBall.x - ball.x;
        const deltaY = this.movingBall.y - ball.y;
        // distance to długość wektora delta czyli odległość między ball a movingBall
        const distance = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
        // sprawdzamy, czy piłeczki nachodzą na siebie
        if (distance < 2 * this.radius * 0.9) { // 0.9 chodzi o to żeby kuleczka łatwiej weszła
          // desiredDelta to wektor o długości 2r i tym samym kierunku i zwrocie, co wektor delta
          // najpierw dzielimy wektor przez jego aktualną długość, a potem mnożymy przez długość, którą powinien był mieć (skalowaie)
          const desiredDeltaX = deltaX / distance * 2 * this.radius;
          const desiredDeltaY = deltaY / distance * 2 * this.radius;
          // cofamy piłeczkę, żeby była w odległości 2r od tej, z którą się zderzyła
          this.movingBall.x = ball.x + desiredDeltaX;
          this.movingBall.y = ball.y + desiredDeltaY;
          this.stopMovingBall();
          return; // wychodzi z metody, żeby nie było krzyku o dwie piłeczki
        }
      }
    }
  }

  stopMovingBall() {
    const rowNumber = Math.round((this.movingBall.y - this.radius) / (this.distanceBetweenBallsY));
    const shiftX = ((rowNumber + this.row) % 2) * this.radius;
    const columnNumber = Math.round((this.movingBall.x - shiftX) / (this.distanceBetweenBallsX));
    /* przeskoczenie na najbliżsżą dozwoloną pozycję dolatującej kulki. Math.random - zaokrąglenie do najbliższej całkowitej.
    Tutaj zaokrąglam do najbliższej wielokrotności 2r */
    this.movingBall.y = rowNumber * this.distanceBetweenBallsY + this.radius;
    this.movingBall.x = columnNumber * this.distanceBetweenBallsX + shiftX;
    this.addBall(this.movingBall);
    if (this.movingBall.group.length >= 3) {
      // filter zwraca fragment listy spełniającej dany warunek
      this.board = this.board.filter(ball => !this.movingBall.group.includes(ball));
      this.removeFreeBalls();


      if (!this.board.find(ball => ball.color === this.movingBall.color)) {
        // zwracam wszystkie kulki , które nie są do usunięcia. Include czy zawiera dany element
        this.colors = this.colors.filter(color => color !== this.movingBall.color);
      }
    } else {
      this.lost += 1;
      if (this.lost > 4) {
        this.addTopRaw();
        this.lost = 0;
      }
    }
    this.testFinishGame();

    this.movingBall = null;
    this.addMiddleBall();
  }

  addBall(ballToAdd: Ball) {

    this.board.push(ballToAdd);

    const sameColourGroups: Ball[][] = [];

    for (const ball of this.board) {
      if (Math.abs(ball.x - ballToAdd.x) <= this.distanceBetweenBallsX * 1.1 &&
        Math.abs(ballToAdd.x - ball.x) <= this.distanceBetweenBallsX * 1.1 &&
        Math.abs(ball.y - ballToAdd.y) <= this.distanceBetweenBallsY * 1.1 &&
        Math.abs(ballToAdd.y - ball.y) <= this.distanceBetweenBallsY * 1.1
      ) {
        if (ballToAdd.color === ball.color && !sameColourGroups.includes(ball.group)) {
          sameColourGroups.push(ball.group);
        }
      }
    }
    const groupColourBalls: Ball[] = [];

    for (const group of sameColourGroups) {
      groupColourBalls.push(...group);      // ... dodają wszystkie elementy
    }

    // grupy stykające się w tym samym kolorze, muszę połączyć te grupy czyli muszę mieć super grupe
    for (const ball of groupColourBalls) {
      // piłeczka ma w sobie grupę w której jest tylko ona (listę). Od teraz ta grupa to to samo co
      ball.group = groupColourBalls;
    }
    // this.superGroupSameColourBalls.push(ball.group);
  }
  // znajdź górnych sąsiadów piłeczki
  isSupported(ballToCheck: Ball) {
    if (ballToCheck.y === this.radius) { return true; }
    for (const ball of this.board) {
      // jeżeli znalazłam wspierającą piłeczkę to prawda
      if (ball.y < ballToCheck.y &&
        // *1.1 komputer nie robi dokładnych porównań, więc to jest takie zabezpieczenie przed zaokrągleniem.
        // Y są trochę zaokrąglone. Problem jest jak mamy liczby zmiennoprzecinkowe
        Math.abs(ballToCheck.y - ball.y) <= this.distanceBetweenBallsY * 1.1 &&
        Math.abs(ballToCheck.x - ball.x) <= this.distanceBetweenBallsX * 1.1
      ) {
        return true; // wychodzi z metody/funkcji!! return zawsze wychodzi z całej funkcji!
      }
    }
    return false;
  }

  removeFreeBalls() {
    while (true) { // taka pętla zawsze się kręci
      const listOfBallToRemove: Ball[] = [];
      for (const ball of this.board) {
        if (!this.isSupported(ball)) {
          listOfBallToRemove.push(ball);
        }
      }
      if (listOfBallToRemove.length === 0) {
        break; // break przerywa pętle
      }
      // zwracam wszystkie kulki , które nie są do usunięcia. Include czy zawiera dany element
      this.board = this.board.filter(ball => !listOfBallToRemove.includes(ball));
    }
  }

  /**** metoda uaktualniająca stan gry, gdzie się ruszyły jakie piłeczki i przerysowująca plansze ****/
  animate(currentTime: number, previousTime: number) {
    this.update(currentTime - previousTime);
    this.drawAll();
    // sprawia, że komputer będzie rysował następną klatkę. To z przeglądarki.
    if (this.movingBall) {
      /** tutaj chodzi o to, że przeglądarka przerysowuje jak najczęściej.
       *  Na koniec narysowania jednej klatki, prosimy, żeby narysować drugą  **/
      window.requestAnimationFrame((nextTime: number) => this.animate(nextTime, currentTime));
    }
  }

  testFinishGame() {
    for (const ball of this.board) {
      if (ball.y > this.boardHeight - this.radius) {
        this.victoryStatus = VictoryStatus.looser;
        break;
      }
    }
    if (this.colors.length === 0) {
      this.victoryStatus = VictoryStatus.winner;
    }
  }
}
